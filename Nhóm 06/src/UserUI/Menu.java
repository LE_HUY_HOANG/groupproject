/*
 * This class for Menu Design
 * 
 */
package UserUI;

import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class Menu {
    protected Stage inputStage = new Stage();
    
    /* Method for Line  Break*/
    protected static Region lineBreak() {
        return new Region() {{
            setPrefSize(Double.MAX_VALUE, 0.0);
        }};
    }
    
    /* Others Methods for Show,Hide or Close Menu Box */
    public void showMenu() {
        inputStage.show();
    }
    
    public void hideMenu() {
        inputStage.hide();
    }
    
    public void closeMenu() {
        inputStage.close();
    }
    
    /* Getter */
    public Stage getInputStage() {
        return inputStage;
    }
}
