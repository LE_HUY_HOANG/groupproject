/*
 * Show a Warning Box if occured Error from input or output
 * 
 */
package UserUI;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ErrorShow {
    private final Stage errorShow = new Stage();

    /* Constructor */
    public ErrorShow(Exception... exception) {
        VBox vBox = new VBox();
        
        for(Exception e : exception) {
            vBox.getChildren().addAll(new Label(e.getMessage()), lineBreak());		/* Add Exception Component to Box */
        }
        
        /* Design for Error Box */ 
        vBox.setPadding(new Insets(15, 15, 15, 15));			      
        errorShow.setScene(new Scene(vBox, 200, 80));
    }
    
    /* Method for Line  Break*/
    private static Region lineBreak() {
        return new Region() {{
            setPrefSize(Double.MAX_VALUE, 0.0);
        }};
    }

    /* Others Methods for Show,Hide or Close Error Box */
    public void showError() {
        errorShow.show();
    }

    public void hideError() {
        errorShow.hide();
    }

    public void closeError(){
        errorShow.close();
    }
}
