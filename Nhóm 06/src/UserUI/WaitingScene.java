/*
 * A UI Box when waiting the program process and search for data
 * 
 */
package UserUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.animation.RotateTransition;
import javafx.util.Duration;

public class WaitingScene {
    private static final Stage waitingScene = new Stage();

    /* Constructor: Design Waiting Box */
    public WaitingScene(String string) {

        Rectangle circle = new Rectangle(100, 100, 100, 100);
        //Creating a rotate transition
        RotateTransition rotateTransition = new RotateTransition();

        //Setting the duration for the transition
        rotateTransition.setDuration(Duration.millis(1000));

        //Setting the node for the transition
        rotateTransition.setNode(circle);

        //Setting the angle of the rotation
        rotateTransition.setByAngle(90);

        //Setting the cycle count for the transition
        rotateTransition.setCycleCount(99999999);

        //Setting auto reverse value to false
        rotateTransition.setAutoReverse(false);

        //Playing the animation
        rotateTransition.play();
        VBox vBox = new VBox(60);
        vBox.getChildren().addAll(circle, new Label(string));
        vBox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(vBox, 400, 250);
        waitingScene.setScene(scene);
        waitingScene.setResizable(false);

        circle.setFill(Color.OLIVE);
    }

    /* Others Methods for Show, Hide or Close Waiting Box */
    public void showScene() {
        waitingScene.show();
    }

    public void hideScene() {
        waitingScene.hide();
    }

    public void closeScene() {
        waitingScene.close();
    }
    
    /* A Method for stop the process when close Waiting Box */
    public void bondWithThread(Thread thread) {
        waitingScene.setOnCloseRequest(windowEvent -> thread.stop());
    }
}
