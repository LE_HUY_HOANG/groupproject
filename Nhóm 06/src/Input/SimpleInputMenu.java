package Input;

import UserUI.Menu;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * 
 * A simple input menu just as the name suggest
 * It have only one text field for inputting keywords
 * 
 */
public class SimpleInputMenu extends Menu {
    //Elements needed for the menu
    private final Button submit = new Button("Xác nhận");
    private final Button more = new Button("Nâng cao");
    private final TextField input = new TextField();
    
    //Constructor
    public SimpleInputMenu() {
        //Show two button submit and more
        HBox hBox = new HBox(5);
        hBox.getChildren().addAll(more,submit);
        hBox.setAlignment(Pos.CENTER);

        //Show the instructions and textfield to input
        VBox vBox = new VBox(5);
        input.setMaxWidth(400);
        vBox.getChildren().addAll(new Label("Nhập các từ khoá để tìm kiếm (Chỉ tìm kiếm trong tiêu đề, khái quát và nội dung) :"), Menu.lineBreak(),
                new Label("Khi muốn nhập nhiều từ khoá, hãy dùng \",\" để ngăn giữa các từ khoá"), Menu.lineBreak(),
                input, Menu.lineBreak(),hBox);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(15,15,15,15));

        //Show the simple stage
        inputStage.setScene(new Scene(vBox,400,150));
        inputStage.setResizable(false);
    }

    //Getters
    public Button getSubmit() {
        return submit;
    }
    
    public Button getMore() {
        return more;
    }
    
    public TextField getInput() {
        return input;
    }
    
}