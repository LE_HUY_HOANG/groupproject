package Input;

import UserUI.Menu;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * 
 * A simple menu but a little bit more complicate
 * It have one text field for inputting keywords like SimpleInputMenu
 * But with extra filters: date, author, source, field
 *
 */
public class ComplicateInputMenu extends Menu {
    //Elements needed for the menu
    private final Button submit = new Button("Xác nhận");
    private final Button less = new Button("Đơn giản");
    private final TextField input = new TextField("");

    // input day, month and year in time
    private final TextField day = new TextField("");
    private final TextField month = new TextField(""); 
    private final TextField year = new TextField("");  
    
    //input author
    private final TextField author = new TextField("");

    //Source of the sentence
    private final CheckBox sourceVNExpress = new CheckBox("VNExpress");
    private final CheckBox sourceCafeF = new CheckBox("CafeF");
    private final CheckBox sourceTNCK = new CheckBox("TNCK");

    //Field of the keyword
    private final CheckBox title = new CheckBox("Tiêu đề");
    private final CheckBox description = new CheckBox("Khái quát");
    private final CheckBox content = new CheckBox("Nội dung");
    
    //Constructor
    public ComplicateInputMenu() {
        //Show two button submit and less
        HBox hBox = new HBox(5);
        hBox.getChildren().addAll(less,submit);
        hBox.setAlignment(Pos.CENTER);

       //Show the complicate stuff about the filter
        VBox inform = new VBox(15);
        inform.getChildren().addAll(new Label("Thời gian(DD-MM-YYYY):"),new Label("Tác giả : "),
                new Label("Nguồn : "),new Label(),new Label("Tìm kiếm trong"));

        //add item to the timebox in the information
        HBox time = new HBox();

        day.setMaxWidth(60);
        month.setMaxWidth(60);
        year.setMaxWidth(120);

        time.getChildren().addAll(day,new Label("-"),month, new Label("-"),year);

        VBox information = new VBox(5);
        information.getChildren().addAll(time,author,sourceCafeF,sourceTNCK,sourceVNExpress,title,description,content);

        BorderPane filter = new BorderPane();
        filter.setLeft(inform);
        filter.setRight(information);

        //Show the instructions and textfield to input
        VBox vBox = new VBox(5);
        input.setMaxWidth(400);
        vBox.getChildren().addAll(new Label("Nhập các từ khoá để tìm kiếm (Chỉ tìm kiếm trong tiêu đề, khái quát và nội dung):"), Menu.lineBreak(),
                new Label("Khi muốn nhập nhiều từ khoá, hãy dùng \",\" để ngăn giữa các từ khoá"), Menu.lineBreak(),
                input, Menu.lineBreak(),
                new Label("Lọc theo : "), Menu.lineBreak(),
                filter, Menu.lineBreak(),
                hBox);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(15,15,15,15));

        //Edit the stage
        inputStage.setScene(new Scene(vBox,420,400));
        inputStage.setResizable(false);
    }

    //Getters
    public Button getSubmit() {
        return submit;
    }

    public Button getLess() {
        return less;
    }

    public TextField getInput() {
        return input;
    }

    public TextField getDay() {
        return day;
    }

    public TextField getMonth() {
        return month;
    }

    public TextField getYear() {
        return year;
    }

    public TextField getAuthor() {
        return author;
    }

    public CheckBox getSourceVNExpress() {
        return sourceVNExpress;
    }

    public CheckBox getSourceCafeF() {
        return sourceCafeF;
    }

    public CheckBox getSourceTNCK() {
        return sourceTNCK;
    }

    public CheckBox getTitle() {
        return title;
    }

    public CheckBox getDescription() {
        return description;
    }

    public CheckBox getContent() {
        return content;
    }

}