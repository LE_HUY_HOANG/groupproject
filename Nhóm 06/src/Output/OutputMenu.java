package Output;

import UserUI.Menu;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
public class OutputMenu extends Menu {

    //-----------------Set checkbox field for output menu----------------------
    private CheckBox author = new CheckBox("Tác giả");
    private CheckBox title = new CheckBox("Tiêu đề");
    private CheckBox date = new CheckBox("Thời gian");
    private CheckBox description = new CheckBox("Khái quát");
    private CheckBox content = new CheckBox("Nội dung");
    private CheckBox source = new CheckBox("Nguồn");

    //---------------------Button submit to show result-------------------------
    private Button submit = new Button("Xác nhận");

    //-----------------------Collect number of records--------------------------
    private int numberOfRecords;
    
    //------------------------Output menu controller----------------------
    public OutputMenu(int i){
        numberOfRecords = i;
        if(numberOfRecords > 0) {

            // Condition and notice to check which field to display
            VBox vBoxTop = new VBox();
            vBoxTop.getChildren().addAll(new Label("Tìm thấy " + numberOfRecords + " kết quả"), Menu.lineBreak(),
                    new Label("Xin hãy chọn các mục sẽ hiện: "), Menu.lineBreak());
            vBoxTop.setAlignment(Pos.TOP_LEFT);

            // Show field to choose
            VBox vBoxLeft = new VBox(5);
            vBoxLeft.getChildren().addAll(author, source, date);
            vBoxLeft.setAlignment(Pos.TOP_LEFT);

            VBox vBoxRight = new VBox(5);
            vBoxRight.getChildren().addAll(title, description, content);
            vBoxRight.setAlignment(Pos.TOP_LEFT);

            VBox vBoxBottom = new VBox(5);
            vBoxBottom.getChildren().addAll(submit);
            vBoxBottom.setAlignment(Pos.CENTER);

            //General UI of output menu
            BorderPane borderPane = new BorderPane();
            borderPane.setLeft(vBoxLeft);
            borderPane.setRight(vBoxRight);
            borderPane.setTop(vBoxTop);
            borderPane.setBottom(vBoxBottom);
            borderPane.setPadding(new Insets(15,15,15,15));
            inputStage.setScene(new Scene(borderPane, 230, 160));
        } else {
            //Notice screen when none data collecting
            VBox vBoxTop = new VBox();
            vBoxTop.getChildren().addAll(new Label("Không tìm thấy kết quả phù hợp"), Menu.lineBreak());
            vBoxTop.setAlignment(Pos.CENTER);
            vBoxTop.setPadding(new Insets(15,15,15,15));
            inputStage.setScene(new Scene(vBoxTop,250,70));
        }
    }
    
    public OutputMenu() {
    	
    }

    //-------------------------Getter and Setter--------------------------------
    public CheckBox getAuthor() {
        return author;
    }

    public CheckBox getTitle() {
        return title;
    }

    public CheckBox getDate() {
        return date;
    }

    public CheckBox getDescription() {
        return description;
    }

    public CheckBox getContent() {
        return content;
    }

    public CheckBox getSource() {
        return source;
    }

    public Button getSubmit() {
        return submit;
    }
    
    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public void setOutputMenu(int i) {
        numberOfRecords = i;
        if(numberOfRecords > 0) {

            VBox vBoxTop = new VBox();
            vBoxTop.getChildren().addAll(new Label("Tìm thấy " + numberOfRecords + " kết quả"), Menu.lineBreak(),
                    new Label("Xin hãy chọn các mục sẽ hiện: "), Menu.lineBreak());
            vBoxTop.setAlignment(Pos.TOP_LEFT);

            VBox vBoxLeft = new VBox(5);
            vBoxLeft.getChildren().addAll(author, source, date);
            vBoxLeft.setAlignment(Pos.TOP_LEFT);

            VBox vBoxRight = new VBox(5);
            vBoxRight.getChildren().addAll(title, description, content);
            vBoxRight.setAlignment(Pos.TOP_LEFT);

            VBox vBoxBottom = new VBox(5);
            vBoxBottom.getChildren().addAll(submit);
            vBoxBottom.setAlignment(Pos.CENTER);

            BorderPane borderPane = new BorderPane();
            borderPane.setLeft(vBoxLeft);
            borderPane.setRight(vBoxRight);
            borderPane.setTop(vBoxTop);
            borderPane.setBottom(vBoxBottom);
            borderPane.setPadding(new Insets(15,15,15,15));
            inputStage.setScene(new Scene(borderPane, 230, 160));
        } else {
            VBox vBoxTop = new VBox();
            vBoxTop.getChildren().addAll(new Label("Không tìm thấy kết quả phù hợp"), Menu.lineBreak());
            vBoxTop.setAlignment(Pos.CENTER);
            vBoxTop.setPadding(new Insets(15,15,15,15));
            inputStage.setScene(new Scene(vBoxTop,250,70));
        }
    }
}

