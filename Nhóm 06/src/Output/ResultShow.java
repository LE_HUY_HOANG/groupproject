package Output;

import Processing.InputExecute;
import Processing.Record;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicInteger;

public class ResultShow {
    //-----------------------Stage display result---------------------------
    private Stage resultShow = new Stage();

    //-----------------------Pagination buttons-----------------------------
    private final Button before = new Button("<<<Hiển thị 30 kết quả trước");
    private final Button after = new Button("Hiển thị 30 kết quả sau>>>");

    public ResultShow() {
    }

    //--------------------Scrollpane and Pagination--------------------------
    public ResultShow(InputExecute inputExecute, OutputMenu outputMenu){
        resultShow.setResizable(false);
        AtomicInteger i = new AtomicInteger();

        //Create scrollpane
        ScrollPane scrollPane = getResut(inputExecute, i.get(),outputMenu);
        resultShow.setScene(new Scene(scrollPane, 1300, 650));

        // Move to next pagination
        before.setOnAction(beforeEvent -> {
            i.addAndGet(-30);

            ScrollPane scrollPane1 = getResut(inputExecute, i.get(),outputMenu);
            resultShow.setScene(new Scene(scrollPane1, 1300, 650));
        });

        // Turn back last pagination
        after.setOnAction(afterEvent -> {
            i.addAndGet(30);

            ScrollPane scrollPane1 = getResut(inputExecute, i.get(),outputMenu);
            resultShow.setScene(new Scene(scrollPane1, 1300, 650));

        });
    }

    //--------------------Main result screen--------------------------
//
    public void showResult(){
        resultShow.show();
    }

    public void hideResult(){
        resultShow.hide();
    }

    public void closeResult(){
        resultShow.close();
    }

    private ScrollPane getResut(InputExecute inputExecute, int i, OutputMenu outputMenu){

        //Create scrollpage to display result
        ScrollPane scrollPane = new ScrollPane();
        int counter = 0;
        before.setPrefWidth(150);
        after.setPrefWidth(150);
        HBox selection = new HBox(20);
        selection.getChildren().addAll(before,after);

        //Create VBox field to display result respectively
        VBox index = new VBox(2);
        VBox author = new VBox(2);
        VBox source = new VBox(2);
        VBox date = new VBox(2);
        VBox title = new VBox(2);
        VBox description = new VBox(2);
        VBox content = new VBox(2);

        //Get data from record to display with respectively label
        while (counter <30) {

            // Set 30 result per screen
            before.setVisible(i != 0);
            after.setVisible(i + 30 < inputExecute.getRecords().size());
            if ( i + counter  >= inputExecute.getRecords().size() ) break;
            else {
                Record record = inputExecute.getRecords().get(i+counter);
                index.getChildren().addAll(new Label((i + counter + 1) + ""));
                counter++;
                if (outputMenu.getAuthor().isSelected()) author.getChildren().addAll(new Label(record.getAuthor()));

                if (outputMenu.getSource().isSelected()) source.getChildren().addAll(new Label(record.getSource()));

                if (outputMenu.getDate().isSelected()) date.getChildren().addAll(new Label(record.getDate()));

                if (outputMenu.getTitle().isSelected()) {
                    String string = record.getTitle();
                    // Clear unnecessary space
                    for (String substring : inputExecute.getTags()) {
                        if(!substring.equals("") && !substring.equals(" ")) {
                            string = string.replaceAll("(?i)" + substring, "<<" + "" + substring + ">>");
                        }
                    }
                    title.getChildren().addAll(new Label(string));
                }

                if (outputMenu.getDescription().isSelected()) {
                    String string = record.getDescription();
                    // Highlight the keyword in descriptions
                    for (String substring : inputExecute.getTags()) {
                        if(!substring.equals("") && !substring.equals(" ")) {
                            string = string.replaceAll("(?i)" + substring, "<<" + substring + ">>");
                        }
                    }
                    description.getChildren().addAll(new Label(string));
                }

                if (outputMenu.getContent().isSelected()) {
                    String string = record.getContent();
                    // Highlight the keyword in content
                    for (String substring : inputExecute.getTags()) {
                        if (string != null) {
                            if(!substring.equals("") && !substring.equals(" ")) {
                                string = string.replaceAll("(?i)" + substring, "<<" + substring + ">>");
                            }
                        }
                    }
                    content.getChildren().addAll(new Label(string));
                }
            }
        }

        //Display result with max 30 result per screen
        HBox summarize = new HBox(8);
        summarize.getChildren().addAll(index, author, source, date, title, description, content);

        VBox result = new VBox(5);
        result.getChildren().addAll(new Label("Hiển thị 30 kết quả từ " + (1 + i) + " tới " + (i + counter ) + " trên tổng số " + inputExecute.getRecords().size() + " kết quả"), selection,summarize);
        scrollPane.setContent(result);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        return scrollPane;
    }
    
    //-----------------------Getter and Setter-------------------------------
    public Stage getResultShow() {
        return resultShow;
    }

    public void setResultShow(ResultShow resultShow){
        this.resultShow = resultShow.getResultShow();
    }
}
