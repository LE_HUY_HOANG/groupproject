package Output;

import UserUI.Menu;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ContinueMenu extends Menu {
    //---------------------Buttons in continue menu--------------------------
    private final Button cont = new Button("Tìm kiếm tiếp");
    private final Button close = new Button("Đóng chương trình");

    //---------------------Config UI for continue menu------------------------
    public ContinueMenu() {
        cont.setPrefWidth(150);
        close.setPrefWidth(150);
        HBox selection =new HBox(10);
        selection.getChildren().addAll(cont,close);
        selection.setPadding(new Insets(15,15,15,15));

        inputStage.setScene(new Scene(selection,350,50));
        inputStage.setResizable(false);
    }
    
    //-------------------------Getter and Setter------------------------------
    public Button getCont() {
        return cont;
    }

    public Button getClose() {
        return close;
    }

}
