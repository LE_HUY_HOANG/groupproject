/*
 * This class convert users's input 's date from string to int
 * And it check if date input is valid
 * 
 */
package Processing;

public class DateCompile {
    private String dateformat1 ;	/* Form of date: dd-mm-yyyy */
    private String dateformat2 ;	/* Form of date: dd/mm/yyyy */
    
    /* Constructor */
    public DateCompile(){

    }
    
    public DateCompile(String day, String month, String year) throws Exception{
        day = day.trim();
        month = month.trim();
        year = year.trim();
        
        if(day.equals("") && month.equals("") && year.equals("")) {
        	/* If user do not input date */
            dateformat1 = "";
            dateformat2 = "";
        } else {
            try {
                ToInt(year);										/* Convert input year */
            } catch (NumberFormatException yearException) {
                throw new Exception("Năm không hợp lệ!");
            }

            try {
                if (ToInt(month) > 12) {							/* Convert and check input month */
                	throw new Exception("Tháng không hợp lệ!");
                }
            } catch (NumberFormatException monthException) {
                throw new Exception("Tháng không hợp lệ!");
            }

            try {
                if (ToInt(month) == 2) {							
                    if ((ToInt(year) % 4 == 0) && (ToInt(day) > 29)) {		/* Check day of Feb if leap year */
                        throw new Exception("Ngày không hợp lệ!");
                    } else if ((ToInt(year) % 4 != 0) && (ToInt(day) > 28)) {	/* Check day of Feb if Ordinary year */
                        throw new Exception("Ngày không hợp lệ!");
                    }
                } else if ((ToInt(month) == 4) || (ToInt(month) == 6) 
                    	|| (ToInt(month) == 9) 
                    	|| (ToInt(month) == 11)) {
                    if (ToInt(day) > 30) {								/* Check day of 30 days - month */
                        throw new Exception("Ngày không hợp lệ!");
                    }
                } else {
                    if (ToInt(day) > 31) {								/* Check day of 31 days - month */
                        throw new Exception("Ngày không hợp lệ!");
                    }
                }
            } catch (NumberFormatException dayException) {
                throw new Exception("Ngày không hợp lệ!");
            }

            dateformat2 = " " + ToInt(day) + "/" + ToInt(month) + "/" + ToInt(year);	/* Convert to Format 2 */

            if (ToInt(day) < 10) {
            	dateformat1 = "0" + ToInt(day) + "-";				/* Add 0 before 1-number-day for format 1 */
            }
            else {
            	dateformat1 = "" + ToInt(day) + "-";
            }

            if (ToInt(month) < 10) {
            	dateformat1 += "0" + ToInt(month) + "-" + ToInt(year) + "";	/* Add 0 before 1-number-month for format 1 */
            }
            else {
            	dateformat1 += ToInt(month) + "-" + ToInt(year)+"";
            }
        }
    }
    
    /* Method convert from String to Int */
    private int ToInt(String string) throws NumberFormatException{
        int i = Integer.parseInt(string);
        
        if (i <= 0) {
        	throw new NumberFormatException();
        }
        else {
        	return i;
        }
    }
    
    /* Getter and Setter */
    public String getDateformat1() {
        return dateformat1;
    }

    public String getDateformat2() {
        return dateformat2;
    }
    
    public void setDateCompile(DateCompile dateCompile) {
        this.dateformat1 = dateCompile.getDateformat1();
        this.dateformat2 = dateCompile.getDateformat2();
    }
}
