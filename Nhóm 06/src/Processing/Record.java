/*
 * Data type for Input Data 
 * 
 */
package Processing;

public class Record {
    private final String date;
    private final String source;
    private final String author;
    private String title;
    private String description;
    private String content;

    /* Constructor */
    public Record(String input) throws Exception {
        String[] breakDownInput = input.split("\",\"");		/* Split Input field by "," */ 
        
        /* Add Input Data to Class's variables */ 
        this.source = breakDownInput[1];
        this.title = breakDownInput[2];
        this.description = breakDownInput[3];
        this.date = breakDownInput[4];
        this.author = breakDownInput[5];
        
        /* Handle Content field of Input data for nice ouput */ 
        if (breakDownInput[6].length() > 5) {
            this.content = breakDownInput[6].substring(2, breakDownInput[6].length() - 3);
        } else {
            this.content = null;
        }
    }
    
    /* Getter and Setter */
    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public String getSource() {
        return source;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}