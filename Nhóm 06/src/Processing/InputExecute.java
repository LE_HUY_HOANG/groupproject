/*
 * This class is used for Searching data with input tags and choices
 * 
 */
package Processing;

import Input.ComplicateInputMenu;
import Processing.DateCompile;
import Processing.Record;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

public class InputExecute {
    private String[] tags ;
    private final Vector<Record> records = new Vector<>();

    /* Path File */
    final static File fileTNCK = new File("resources\\TNCKArticles.CSV");
    final static File fileCafef = new File("resources\\CafefArticles.CSV");
    final static File fileVnexpress = new File("resources\\VnexpressArticles.CSV");
    Scanner scanner;

    /* Constructor */
    public InputExecute(){

    }
    
    /* Searching in simple menu for input users tags that splited by comma */
    public InputExecute(String input) {
        String[] inputBreakDown = input.split(",");			 /* Split users's input */
        tags = new String[inputBreakDown.length];
        
        for(int i =0;i < inputBreakDown.length; i++) {		 /* Sequentialy assign input to a String array */	
        	tags[i] = inputBreakDown[i].trim();
        }
        
        SearchWithTag(fileCafef, tags);						 /* Use method to search from each file */
        SearchWithTag(fileTNCK, tags);
        SearchWithTag(fileVnexpress, tags);
    }
    
    /* 
     * Searching in complicate menu with input users tags that 
     * splited by comma, date, author and file selected 
     */
    public InputExecute(ComplicateInputMenu complicateInputMenu, DateCompile date) {
        String[] inputBreakDown = complicateInputMenu.getInput().getText().split(",");		 /* Split users's input */
        tags = new String[inputBreakDown.length];
        
        for(int i =0;i < inputBreakDown.length; i++) {
        	tags[i] = inputBreakDown[i].trim();				/* Sequentialy assign input to a String array */	
        }
        
        /* File selected */
        Vector<File> file = new Vector<>();
        if(complicateInputMenu.getSourceCafeF().isSelected()) file.add(fileCafef);			
        if(complicateInputMenu.getSourceTNCK().isSelected()) file.add(fileTNCK);
        if(complicateInputMenu.getSourceVNExpress().isSelected()) file.add(fileVnexpress);
        
        /* 
         * Use method to search with users choices 
         * from date, author, file, field choiced and tags input 
         */
        for(File f: file){
            SearchWithTagUtil(f, tags, date, complicateInputMenu.getAuthor().getText(),
            		complicateInputMenu.getTitle().isSelected(),
            		complicateInputMenu.getDescription().isSelected(),
            		complicateInputMenu.getContent().isSelected());
        }
    }
    
    /* Method for searching following tags from record of each file */
    private void SearchWithTag(File file, String[] tags ) {
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        scanner.nextLine();
        while(scanner.hasNextLine()){
            String nextLine = scanner.nextLine();
            boolean nextLineCorrect = false;
            
            while (!nextLineCorrect) {
                try {
                    Record record = new Record(nextLine);

                    boolean verify = true;
                    
                    /* Find tags from 3 field */
                    for (String tag : tags) {
                        if (!(SearchAString(record.getTitle(), tag) 				 
                        		|| SearchAString(record.getDescription(), tag) 
                        		|| SearchAString(record.getContent(), tag))) {
                            verify = false;											 /* When not found */			
                        }
                    }
                    
                    if (verify) {
                        records.add(record);			 /* When found the tags, add to a Vector<Record> records */
                    }
                    
                     nextLineCorrect = true;
                } catch (Exception e) {
                    nextLine += scanner.nextLine();
                }
            }
        }
    }
    
    /* Method for searching following tags input, author, date, and selected file */
    private void SearchWithTagUtil(File file, String[] tags ,DateCompile date, 
    		String author, boolean title, boolean description, boolean content) {

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        scanner.nextLine();
        boolean stop = false;
        
        while(scanner.hasNextLine() && !stop){
            String nextLine = scanner.nextLine();
            boolean nextLineCorrect = false;
            boolean dayWented = false;
            
            while ( !nextLineCorrect) {
                
                try {
                    Record record = new Record(nextLine);								
                    nextLineCorrect = true;
                    
                    /* Search authors */
                    boolean verify = SearchAString(record.getAuthor(),author);			
                    
                    /* Search date */
                    if((!SearchAString(record.getDate(),date.getDateformat1())) 
                    		&& (!SearchAString(record.getDate(),date.getDateformat2()))) {
                        verify = false;												
                        
                        if(dayWented) {
                        	stop = true;
                        }
                    } else {
                        dayWented = true;
                    }

                    /* Search for field choosen */ 	
                    if(verify) {
                        boolean titleValid = false;
                        boolean descriptionValid = false;
                        boolean contentValid = false;
                        
                        if(title){
                            titleValid = true;						/* If title is choosen */
                            
                            for (String tag : tags) {				/* Search for title */
                                if (!SearchAString(record.getTitle(), tag)) {
                                    titleValid= false;
                                }
                            }
                        }
                        
                        if(description){
                            descriptionValid = true;				/* If description is choosen */
                            
                            for (String tag : tags) {				/* Search for description */
                                if (!SearchAString(record.getDescription(), tag)) {
                                    descriptionValid = false;
                                }
                            }
                        }
                        
                        if(content){
                            contentValid = true;					/* If content is choosen */
                            
                            for (String tag : tags) {				/* Search for cotent */
                                if (!SearchAString(record.getContent(), tag)) {
                                    contentValid = false;
                                }
                            }
                        }

                        if(titleValid || descriptionValid || contentValid) {
                        	records.add(record);					/* Add to record if 1 of 3 field is choosen */
                        }
                    }
                } catch (Exception e) {
                    nextLine += scanner.nextLine();
                }
            }
        }
    }
    
    /* Method for Searching a substring from a string */
    private boolean SearchAString(String string, String substring){
        if(string != null ) {
            return string.toLowerCase().contains(substring.toLowerCase());
        } else {
            return false;
        }
    }
    
    /* Getter */
    public String[] getTags() {
        return tags;
    }

    public Vector<Record> getRecords() {
        return records;
    }
}
