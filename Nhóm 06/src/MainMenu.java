/*
    Lớp này là chương trình chính của project
    Chức năng chính của lớp là kết nối các Menu, bộ xử lý trong project lại với nhau để tạo thành một chương trình
 */

import Input.ComplicateInputMenu;
import Input.SimpleInputMenu;
import Output.ContinueMenu;
import Output.OutputMenu;
import Output.ResultShow;
import Processing.DateCompile;
import Processing.InputExecute;
import UserUI.ErrorShow;
import UserUI.WaitingScene;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.concurrent.*;

public class MainMenu extends Application {

    private InputExecute inputExecute = new InputExecute();

    //Tạo Menu tìm kiếm cơ bản và hiển thị Menu này lên lúc bắt đầu chưởng trình
    SimpleInputMenu simpleInputMenu = new SimpleInputMenu();
    //Tạo menu tìm kiếm nâng cao
    ComplicateInputMenu complicateInputMenu = new ComplicateInputMenu();

    //Tạo cửa sổ chờ trong lúc tìm kiếm để tăng trải nhiệm người dùng
    WaitingScene waitingScene = new WaitingScene("Looking in the database ...");

    //Tạo Menu cho người dùng các lựa chọn tiếp tục chương trình hay dừng lại
    ContinueMenu continueMenu = new ContinueMenu();

    //Tạo Menu hiển thị các mục trong kết quả, hiện menu này đang trống do chưa nhập vào kết quả
    final OutputMenu outputMenu = new OutputMenu();

    //Tạo cửa sổ hiển thị kết quả
    final ResultShow resultShow = new ResultShow();

    //Tạo bộ xử lý ngày
    final DateCompile dateCompile = new DateCompile() ;


    public void start(Stage primaryStage) {

        //Hiển thị Menu nhập đơn giản, coi đây là Menu mặc định
        simpleInputMenu.showMenu();

        //Khi nhấn nút Nâng cao trong simpleInputMenu sẽ chuyển sang complicateInputMenu
        simpleInputMenu.getMore().setOnAction(more ->{
            simpleInputMenu.hideMenu();
            complicateInputMenu.showMenu();
        });

        //Khi nhấn nút submit trong simpleInputMenu
        simpleInputMenu.getSubmit().setOnAction(simpleInput -> {
            //Ẩn simpleInput Menu
            simpleInputMenu.hideMenu();

            //Tạo task để chạy trong nền do JavaFX sẽ bị đơ khi xử lí dữ liệu lớn và chiếu Animation cùng lúc
            Task <InputExecute> simpleTask = new Task<>() {
                @Override
                protected InputExecute call() {
                    //Lấy string input từ dữ liệu trong simpleInputMenu
                    String input = simpleInputMenu.getInput().getText();

                    //Tạo biến inputExecute, constructer của inputExecute với biến tham chiếu là string input
                    //constructor sẽ xử lý số liệu và lưu lại các bản ghi hợp lệ trong thuộc tính records của inputExecute
                    InputExecute inputExecute = new InputExecute(input);

                    //trả lại kết quả cho task
                    return inputExecute;
                }
            };

            //Tạo thread để chạy Task
            Thread thread = new Thread(simpleTask);
            thread.setDaemon(true);
            thread.start();

            //Trong lúc đang chạy task, màn hình sẽ hiển thị cửa sổ chờ
            waitingScene.showScene();

            //Kết nối cửa sổ chờ với thread đang chạy để nếu tắt thread thì sẽ tắt chương trình
            waitingScene.bondWithThread(thread);

            //khi task chạy xong
            simpleTask.setOnSucceeded(succeeced-> {
                //lấy kết quả đã được xử lý inputExucute từ trong task
                inputExecute = simpleTask.getValue();

                //đóng cửa sổ chờ
                waitingScene.closeScene();

                //gọi method khởi tạo outputMenu với số liệu truyền vào là số lượng bản ghi hợp lệ( kích cỡ thuộc tính records) trong input Execute
                outputMenu.setOutputMenu(inputExecute.getRecords().size());

                //hiển thị ouputMenu
                outputMenu.showMenu();
            });
        });

        //Bấm nút Đơn giản trong complicateInputMenu sẽ chuyển sang simpleInputMenu
        complicateInputMenu.getLess().setOnAction(less -> {
            complicateInputMenu.hideMenu();
            simpleInputMenu.showMenu();
        });

        //Khi bấm xác nhận trong complicateInputMenu
        complicateInputMenu.getSubmit().setOnAction(complicateInput -> {

            //Kiểm tra ngày tháng đã được nhập đúng chưa
            boolean rightDayInput ;
            try {
                //gọi method khởi tạo dateCompile dựa trên 3 string ngày, tháng ,năm được nhập trong complicateInputMenu
                //method này sẽ ném ra các Exception trong trường hợp nhập ngày tháng năm chưa chính xác
                dateCompile.setDateCompile( new DateCompile(complicateInputMenu.getDay().getText(), complicateInputMenu.getMonth().getText(), complicateInputMenu.getYear().getText()));

                //nếu không có Exception nào bị ném ra thì ngày tháng năm nhập đã chính xác
                rightDayInput = true;

            } catch (Exception e) {
                //Nếu có Exception bị ném ra
                //Tạo màn hình hiển thị lỗi (class ErrorShow) với biến tham chiếu truyền vào là lỗi bắt được
                ErrorShow errorShow = new ErrorShow(e);

                //hiển thị màn hình hiện lỗi
                errorShow.showError();

                //ngày tháng năm nhập chưa chính xác
                rightDayInput = false;
            }

            //chương trình sẽ đi tiếp chỉ khi ngày tháng năm nhập chính xác
            if(rightDayInput) {
                //Ẩn complicateMenu đi
                complicateInputMenu.hideMenu();

                //Tạo task để chạy trong nền do JavaFX sẽ bị đơ khi xử lí dữ liệu lớn và chiếu Animation cùng lúc
                Task<InputExecute> complicateTask = new Task<>() {
                    @Override
                    protected InputExecute call() {
                        //Tạo biến inputExecute, constructer của inputExecute với biến tham chiếu
                        //complicateInputMenu chứa các lựa chọn của người dùng
                        //dateComplile chứa ngày tháng năm đã được xử lý
                        //constructor sẽ xử lý số liệu và lưu lại các bản ghi hợp lệ trong thuộc tính records của inputExecute
                        InputExecute inputExecute = new InputExecute(complicateInputMenu,dateCompile);

                        //trả lại kết quả cho task
                        return inputExecute;
                    }
                };
                //Tạo thread để chạy task
                Thread thread = new Thread(complicateTask);
                thread.setDaemon(true);
                thread.start();

                //Trong khi chạy task sẽ hiện của sổ chờ
                waitingScene.showScene();

                //Kết nối cửa sổ chờ với thread đang chạy để nếu tắt thread thì sẽ tắt chương trình
                waitingScene.bondWithThread(thread);

                //khi task chạy xong
                complicateTask.setOnSucceeded(succeeced -> {
                    //lấy dữ liệu đã được xử lý từ trong task
                    inputExecute = complicateTask.getValue();

                    //đóng cửa sổ chờ
                    waitingScene.closeScene();

                    //gọi method khởi tạo outputMenu với số liệu truyền vào là số lượng bản ghi hợp lệ( kích cỡ thuộc tính records) trong input Execute
                    outputMenu.setOutputMenu(inputExecute.getRecords().size());

                    //hiển thị outputMenu
                    outputMenu.showMenu();

                });
            }

        });

        //Khi người dùng bấm xác nhận các trường sẽ hiển thị trong dữ liệu
        outputMenu.getSubmit().setOnAction(submit->{
            //đóng cửa sổ lựa chọn trường
            outputMenu.closeMenu();

            //Gọi hàm khởi tạo cửa sổ hiển thị dữ liệu với các tham số :
            //inputExecute chứa các bản ghi hợp lệ
            //outputMenu chứa các lựa chọn của người dùng
            resultShow.setResultShow(new ResultShow(inputExecute,outputMenu));

            //hiển thị cửa sổ lựa chọn tìm kiếm tiếp hay không
            continueMenu.showMenu();

            //hiển thị cửa sổ kết quả
            resultShow.showResult();


        });
        //Nếu người dùng bấm cửa sổ chọn trường (tức chỉ cần biết số lượng kết quả mà thôi)
        //thì hiện cửa sổ lựa chọn tìm kiếm tiếp hay không
        outputMenu.getInputStage().setOnCloseRequest(close -> continueMenu.showMenu());


        //Nếu bấm tiếp tục tìm kiếm trong cửa sổ lựa chọn
        continueMenu.getCont().setOnAction(actionEvent -> {
            //đóng cửa sổ lựa chọn
            continueMenu.closeMenu();

            //đóng cửa sổ hiển thị kết quả
            resultShow.closeResult();

            //hiển thị cửa sổ tìm kiếm đơn giản
            simpleInputMenu.showMenu();
        });

        //Nếu bấm đóng chương trình
        continueMenu.getClose().setOnAction(actionEvent -> {
            //đóng cửa sổ lựa chọn
            continueMenu.closeMenu();

            //đóng cửa sổ hiển thị kết quả
            resultShow.closeResult();
        });
    }
    public static void main(String[] args) {
        launch(args);
    }

}